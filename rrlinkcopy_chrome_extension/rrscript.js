var sources = GetVideoSources();
$.each(sources, function(key, value) {
  $(".mediaDetails").append(CreateLinkCopyParagraph(value));
});

$('.rrcopylink').on("click",function(){
  var texttocopy =  $(this).attr("texttocopy");
  CopyToClipboard(texttocopy);
})

function GetVideoSources() {
	var sources = [];
	var children = $("video").children("source");
		
	// get highest resolution
	var highestres = 0;
	children.each(function() {
		var res = parseInt($(this).attr('res'));
		console.log("current res: " + res + ", highest res: " + highestres);
		if (highestres < res){
			highestres = res;
		}
	});
	
	children.each(function () {
		if (highestres === 0 || $(this).attr('res') === highestres.toString()){
			sources.push($(this).attr("src"));
		}
	});
	return sources;
}	

function CreateLinkCopyParagraph(source) {
	var splitsource = source.split('.');
	var mediatype = splitsource[splitsource.length - 1].toUpperCase();
	var p = "<p><a class=\"rrcopylink\" href=\"javascript:;\" texttocopy=\"" + source + "\">" + mediatype + "</a></p>";
    return p;
}

function CopyToClipboard(text) {
    var $temp = $("<input class='hidden'>");
    $("body").append($temp);
    $temp.val(text).select();
    document.execCommand("copy");
    $temp.remove();
}